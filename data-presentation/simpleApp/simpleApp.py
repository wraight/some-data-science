import streamlit as st

import os
import subprocess


### introduction
st.title('🙂 Simple webApp example')
st.write("---")

### contents
st.write("## Contents")
pyFiles=[f for f in os.listdir(os.getcwd()+"/simpleApp/pages") if ".py" in f] 
pyFiles=sorted(pyFiles)
for e,pf in enumerate(pyFiles,1):
    st.write(f" {e}. {pf}")

st.write("---")

### links
st.write("## Useful Links")
linkList=[
    {'name':"streamlit docs", 'link':"https://docs.streamlit.io"},
    {'name':"emoji docs", 'link':"https://www.webfx.com/tools/emoji-cheat-sheet/"},
    {'name':"markdown docs", 'link':"https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"},
]
for ll in linkList:
    st.write(f"[{ll['name']}]({ll['link']})")

st.write("---")
