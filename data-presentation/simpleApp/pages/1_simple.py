import streamlit as st
import streamlit.components.v1 as components

import os

### introduction
st.title(':clipboard: Simple Page')
st.write('### Basic examples')
st.write("---")

### text
st.write("## Text")
st.write("text is written in markdown so the _usual_ sorts of __highlights__ are possible.")
st.write(" - which is nice ")
st.write("---")

### writing on the sidebar
st.sidebar.markdown("Some sidebar text too!")

### images
st.write("## Images")
st.write("images can be included")
st.image(os.getcwd()+"/simpleApp/enscription.jpg", caption="with optional caption") 
st.write("---")

### embedding
st.write("## Embedding")

### html
st.write("Custom _html_ can be added")
components.html("<p> some custom html </p>")

### web pages
st.write("embedding web pages is also possible")
html="https://www.webfx.com/tools/emoji-cheat-sheet/"
components.iframe(html, width=800, height=400, scrolling=True)

st.write("---")


