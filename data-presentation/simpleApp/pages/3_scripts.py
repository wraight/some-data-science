import streamlit as st

import os
import sys
import json
import subprocess
import altair as alt
import pandas as pd

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/simpleApp/scripts')
from simpleFunctions import GetInventory, AnalyseText

### introduction
st.title(':link: Scripts')
st.write('### Upload a file and run a script')
st.write("---")


### scripts
st.write("## Simple scripts")
st.write("A couple of examples (files in folder)")

st.write("### 1. Run script from file")
pySel=os.getcwd()+"/simpleApp/scripts/simpleScripts.py"

st.write(f"Use file: {pySel}")
st.write("Run as _subprocess_:")
st.write(f"> python3 {pySel}")

cmdArr=['python3',pySel]

if st.button("Run subprocess!"):
    st.write("Running...")
    session = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = session.communicate()

    if stdout:
        st.success("subprocess success!")
        st.write(stdout.decode("utf-8"))

    if stderr:
        st.error("Error "+stderr.decode("utf-8"))


st.write("### 2. Run script from function (in file)")
st.write("import function from file")

if st.button("Run function!"):
    st.write("Running...")
    st.write(GetInventory())


st.write("---")

### upload file
st.write("## Upload file")
st.write("Upload file with _json_ or _txt_ extensions")

upFile=st.file_uploader("Upload file",type=['json','txt','dat'])

if upFile==None:
    st.write("No file added, so stop.")
    st.stop()
else:
    st.write({"FileName":upFile.name,"FileType":upFile.type})
    bytes_data = upFile.getvalue()
    data = upFile.getvalue().decode('utf-8')  
    if "json" in upFile.type.lower():
        st.write("_json_ type")
        st.json(data)
    else:
        st.write("__not__ _json_ type")
        st.write(data)



st.write("---")

st.write("## Analyse File")
st.write("Simple analysis of uploaded file")

countDict=AnalyseText(data)

df_count=pd.DataFrame([{'name':k,'count':v}for k,v in countDict.items()])
df_count['type']=df_count['name']

def charType(x):
    if x.isnumeric():
        return "numeric"
    elif x.isalpha():
        return "alpha"
    else:
        return "non_AN"
    
df_count['type']=df_count['name'].apply(lambda x: charType(x))

st.write(df_count.transpose())

for t in df_count['type'].unique():
    df_plot=df_count.query('type=="'+t+'"')
    st.write(
        alt.Chart(df_plot).mark_bar().encode(
        y='name',
        x='count',
        color=alt.condition( # highlight largest
        alt.datum.count >= df_plot['count'].max(),   # define condition
        alt.value('green'), # if condition True
        alt.value('steelblue') # if condition False
        )
        ).properties(title=t)
    )
