import streamlit as st

import os
import sys
import json
import subprocess
import altair as alt
import pandas as pd

### introduction
st.title(':link: Caching')
st.write('### Example caching')
st.write("---")

### introduction
st.write("## Background")
st.write("Streamlit executes python code from _top_ to _bottom_ after each interaction.") 
st.write(" - this this means _caching_ is required to retain user inputs after multiple interactions.")
st.write("For Streamlit caching is implemented via the *st.session_state* object.")

st.write("---")

### no caching
st.write("## Without caching")
st.write("The state of widgets will be lost after multiple interactions without caching")

st.write("### no caching example")

# input slider
inVal=st.slider("Pick a number", 0, 10)
st.write(" - Widget keeps value")

st.write(f"You selected: __{inVal}__")
st.write(" - this will stay the same as this statement is _before_ addition")

# increment button
buttonCheck=False
if st.button("Add one!"):
    st.write("Adding to selected value")
    inVal+=1
    buttonCheck=True

st.write(f"Now value is: __{inVal}__")
if buttonCheck:
    st.write(" - repeated button presses will __not__ continue increments without caching")

irrList=['neither', 'pi', 'e']
radVal= st.radio("Favourite irrational number", irrList, index=0)

if radVal!="neither":
    st.write("New interaction has overwitten addition")

st.write(f"You've now got: __{inVal}__")
st.write(" - Is this what you wanted?")

st.write("---")

### with caching
st.write("## With caching")
st.write("### caching example")

# input slider
newVal=st.slider("Pick another number", 0, 10)
st.write(" - Widget keeps value")

st.write(f"You selected: __{newVal}__")

# caching
if "inVal" not in st.session_state.keys() or st.button("update session_state"):
    st.session_state['inVal']=newVal
    st.write("Cache updated!")
else:
    st.write("Cache only updated first time automatically. On demand thereafter.")

# increment button
buttonCheck=False
if st.button("Add another one!"):
    st.write("Adding to selected value")
    st.session_state['inVal']+=1
    buttonCheck=True

st.write(f"Now _cached_ value is: __{st.session_state['inVal']}__")
if buttonCheck:
    st.write(" - repeated button presses will continue increments with caching")


irrList=['neither', 'pi', 'e']
radVal= st.radio("Favourite irrational number (again)", irrList, index=0)

if radVal!="neither":
    st.write("New interaction will __not__ overwite cached addition")

st.write(f"You've now got: {st.session_state['inVal']}")
st.write(" - Is this what you wanted?")

st.write("---")

