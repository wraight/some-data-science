import os

print("This is the inventory of the working directory:\r\r")

files=[f for f in os.listdir(os.getcwd())] 

for e,f in enumerate(files):
    print(f"\t{e}: {f} \r")

print("all done.")
