import os

def GetInventory():
    retStr="This is the inventory of the working directory:\r\r"

    files=[f for f in os.listdir(os.getcwd())] 

    for e,f in enumerate(files):
        retStr+=f"\t{e}: {f} \n"

    retStr+="all done."

    return retStr

def AnalyseText(inStr):
    countDict={}
    for x in inStr:
        try:
            countDict[x]+=1
        except KeyError:
            countDict[x]=1
    
    return countDict
