# some-data-science

A short course?

### Contents

1. Data Tools
    - Notebooks - Jupyter
    - Containerisation - Docker
    - Git - GitHub
2. Databases
    - Backend - Influx
    - Frontend - Grafana
    - Wrangling - Pandas
3. Data Presentation
    - Plotting - Altair, Plotly, Bokeh
    - Web Applications - Streamlit
