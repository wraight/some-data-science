### useful details

def GetInfluxCredentials():
    print("Getting Influx info.")
    return {'bucket':"BUCKET",
            'org':"ORG",
            'token':"TOKEN",
            'url':"URL:PORT"
            }
