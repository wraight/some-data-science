# Databases

Some examples reading data and visualising.

## Simple database

Convert (well formatted) _csv_ file to pandas data pane
- raw data --> queryable object

### Wrangling

Query and plot some data
- pandas dataframe with altair visualisations

## Realistic Databases

Influx, mongoDB

### Set-up TIG Stack

Setting up Influx, Grafana and Telefraf can be done via a container stack (group of containers).

- Original configs
    - https://sweetcode.io/set-up-telegraf-influxdb-and-grafana-with-docker-compose/
- Alternative configs
    - https://medium.com/age-of-awareness/tig-telegraf-influxdb-grafana-stack-automation-with-docker-bf7a1e55218a
- Influx2
https://community.influxdata.com/t/docker-compose-example/25627/3
- Git repo - works!
    - https://github.com/alekece/tig-stack


### Backend - Influx

Query influx database for data
- plotting with altair

### Frontend - Grafana


